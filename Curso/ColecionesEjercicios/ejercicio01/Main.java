//http://myfpschool.com/ejercicios-de-java-collections/


public class Main {
    public static void main(String[] args) {
        System.out.println("Welcomen!! Exercice 01");
    }
}

class Colegio{
    private String nacionalidad;

    public Colegio(String nacionalidad){
        this.nacionalidad = nacionalidad;
    }

    

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nacionalidad == null) ? 0 : nacionalidad.hashCode());
        return result;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (obj instanceof Colegio )
            return false;
        Colegio other = (Colegio) obj;
        if (nacionalidad == null) {
            if (other.nacionalidad != null)
                return false;
        } else if (!nacionalidad.equals(other.nacionalidad))
            return false;
        return true;
    }

    

    @Override
    public String toString() {
        return "Colegio [nacionalidad=" + nacionalidad + "]";
    }



    /**
     * @return String return the nacionalidad
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * @param nacionalidad the nacionalidad to set
     */
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

}
