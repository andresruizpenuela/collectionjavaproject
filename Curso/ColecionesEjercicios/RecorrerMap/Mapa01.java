import java.util.HashMap;
import java.util.*;

public class Mapa01 {
    public static void main(String[] args) {
        HashMap<String, Integer> mapa = new HashMap<>();
         
        mapa.put("producto1", 5);
        mapa.put("producto2", 7);
        mapa.put("producto3", 9);
        mapa.put("producto4", 1);
        mapa.put("producto5", 4);


        //Mediante un forEach
        System.out.println("FOR EACH WITH MAP");
        for(String key: mapa.keySet()){
            System.out.println("Mapa ["+mapa.get(key)+"]");
        }

        //Mediante un iterador
        System.out.println("FOR ITERATO WITH MAP KEYSET");
        Iterator itKey = mapa.keySet().iterator();
        while (itKey.hasNext()){
            Object value = mapa.get(itKey.next());
            System.out.println("Mapa ["+value+"] ");
            
        }
        //Otra de las cosas más útiles al trabajar con los Map es el recorrerlos como si fuese un ArrayList, y eso lo conseguimos de la siguiente forma
        System.out.println("FOR ITERATOR WITH MAP MAP.ENTRY");
        Iterator it = mapa.entrySet().iterator();
        while (it.hasNext()){
            Object x = it.next();

            System.out.println("Mapa ["+x+"] "+(x.getClass()));
            Map.Entry dato = (Map.Entry) x;
            System.out.println("Map.Entry ["+dato.getKey()+", "+dato.getValue()+"]");
            
        }

        //Solo Keys
        System.out.println("FOR de Key");
        for(Object key : mapa.keySet()){
            System.out.println("Key: "+key);
        }

        //Solo valores
        System.out.println("FOR de Values");
        for(Object value : mapa.values()){
            System.out.println("values "+value);
        }
    }
}
