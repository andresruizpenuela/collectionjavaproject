package com.tda.pila.statica;

public interface IPila{
	
	boolean isEmpaty();
	Object pop();
	Object peek();
	void push(Object elem);
	void delete();
	int hasCode();
	String toString();

}
