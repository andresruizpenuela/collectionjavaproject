package com.tda.vector;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TDAVectorText {

	static TDAVector v1;
	static TDAVector v2;
	@BeforeClass
	public static void setUpBeforeClass() {
		v1= new TDAVector();
		v2 = new TDAVector(4);
		
	}

	
	@Test
	public void test1_testAdd_v1() {
		v1.elementData[0]=3;
		v1.elementData[1]=4;
	}
	
	@Test
	public void test2_testAdd_v2() {
		v1.add(1);
		v1.add(2);
		v1.add(3);
		
		v2.add(1);
		v2.add(2);
		v2.add(3);
		v2.add(4);
		v2.add(5);
		

	}
	
	@Test
	public void test3_tesToString() {
		System.out.println("v1= "+Arrays.toString( v1.elementData)+", length: "+v1.elementData.length);
		System.err.println("v2= "+Arrays.toString( v2.elementData)+", length: "+v2.elementData.length);
	}

}
