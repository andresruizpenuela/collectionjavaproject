package com.tda.vector;
/*
 * The com.tda.vector.TVector class is a TDA (Abstrac Type Data) for colletion an array of element dinamic
 */
public class TDAVector {

	//Array element of generic type
	protected Object[] elementData;
	
	//Construcctor for default, this creating an array of length "1" element
	public TDAVector() {
		//this.elementData=new Object[1]; 
	}
	
	//Construcctor with param for creating an array of length "tam" elements
	public TDAVector(int tam) {
		this.elementData=new Object[tam];
	}
	
	/**
	 * The com.tda.vector.TVector.add() method add a new item to array
	 * @param element
	 */
	public void add(Object element) {
		int pos=0
				;
		if(isEmpaty()) {
			this.elementData = new Object[1];
			pos = this.elementData.length-1;
		}else {
			while(pos<this.elementData.length) {
				if(this.elementData[pos]!=null) {
					pos++;
				}else {
					break;
				}
			}
			if(pos>=this.elementData.length) {
				boolean containNull = false;
				Object[] aux = this.elementData;
				this.elementData = new Object[this.elementData.length+1];
				copyTVectpr(aux,this.elementData);
			}
			
		}
				
		this.elementData[pos]=element;

	}
	
	private void copyTVectpr(Object[] v1,Object[] v2) {
        /*
         * The java.lang.System.arraycopy() method copies a source array from a specific 
         * beginning position to the destination array from the mentioned position. No. of arguments to be copied are decided by len argument.
         * Parameters : 
		 *	source_arr : array to be copied from
		 *	sourcePos : starting position in source array from where to copy
		 *	dest_arr : array to be copied in
		 *	destPos : starting position in destination array, where to copy in
		 *	len : total no. of components to be copied.
         */
		System.arraycopy(v1, 0, v2, 0, v1.length);
		
	}
	
	public boolean isEmpaty() {
		return (this.elementData==null)?true:false;
	}
	
	
	public void destoy() {
		this.elementData = null;
	}
	
}
