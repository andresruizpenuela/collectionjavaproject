package com.api.java.testArrays;

import java.util.Vector;

public class TestVector {

	public static void main(String args[]) {
		Vector v1 = new Vector(); //Crea un venctor de 10 elementos, con un autoincremnto de 1 - 11,12,13,14,... elementos.
		Vector v2 = new Vector(15,5);//Crea un venctor de 15 elementos, con un autoincremnto de 5 - 15,20,25,30,... elementos.

		//A�adir elemento
		v2.add(1);
		v2.add(2);
		
		v1.addAll(0,v2);
		
		//ToString
		System.out.println("v1= "+v1.toString());
		System.out.println("v1= [");
		v1.stream().forEach(System.out::println);
		System.out.println("]");
		
		
		//Length y size
		System.out.println("Length "+ v1.toArray().length);
		System.out.println("Size "+v2.size());
	}
}
