package com.api.java.testArrays;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class TestSystem {

	public static void main(String[] args) {
		int[] v1 = {1,2,3,4};
		int[] v2;
		int[] v3;
		int[] v4;
		
		System.out.println("v1");
		mostarArray(v1);
		
		/**
		 * Copiar array - Metodos baiscos
		 */
		System.out.println("System.copyarray - v1 in v2");
		v2 = new int[v1.length];
		System.arraycopy(v1, 0, v2, 0, 0);
		System.out.println("v2");
		mostarArray(v2);
		
		System.out.println("Object.clone - v3 = v1.clone");
		v3 = v1.clone();
		System.out.println("v3");
		mostarArray(v3);
		
		System.out.println("Arrays.copyOf - v4 Arrays.copyOf(v1,2)");
		v4 = Arrays.copyOf(v1, 2);
		System.out.println("v4");
		mostarArray(v4);
		
	}
	
	public static void mostarArray(int[] array) {
		AtomicInteger index = new AtomicInteger();

		System.out.println("array.lenth= "+array.length+", array={");
		Arrays.stream(array).forEach((elemento)->{
			System.out.print("\tElemento "+index+" = ");
			System.out.println(elemento);
			index.getAndIncrement();
		});
		System.out.println("}");
	}
}
