/**
 * 
 */
package tipo_datos.tda.estaticos.pila;

/**
 * Interfaz con las operaciones b�sicas que ha de impletar un TDA Pila
 * @author Andr�s Ruiz Pe�uela
 * @version 0.1
 * @see <a href="http://codigolibre.weebly.com/blog/pilas-en-java">Pilas en Java</a>
 *
 */
public interface IPila {
	void apilar(Object data);
}
