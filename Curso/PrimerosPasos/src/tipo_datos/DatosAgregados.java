package tipo_datos;

public class DatosAgregados {
	//En Java los datos agregados son tratados como Objectos que a su vez hereada de la clase Object
	//y además da soporte algunos algortimos o metodos de operación como length, asiganción, comparación ...
	
	public static void main(String arg[]) {
		int dia[] = {1,2,3,4,5,6,7,8};
		int dias[]; 
		dias = dia; // En C guardiara la posicion de memoria em
		
		System.out.println("dia[0]= "+dia[0]);
		System.out.println("dias[0]= "+dias[0]);
		
		
		//Opera
		System.out.println("dia.length= "+dia.length); //En C este metodo se deberia esribir 
		if(dia == dias) { //En C esto no se podíra dar dado que se compara puneteros, en JAVA se ocmpara dos array de tipo int
			System.out.println("Ok");
		}else {
			System.out.println("No Ok");
		}
	}
}
