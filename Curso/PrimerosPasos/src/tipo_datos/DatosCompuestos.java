package tipo_datos;

//Dato compuesto
class Telefono{
	short pin;	//Dato primitivo
	int numero[] = new int[9];
	

	//Consturctor
	public Telefono() {};
	public Telefono(short pin, int[] numero) {
		this.pin = pin;
		this.numero = numero;
	}

}
	
public class DatosCompuestos {
	
	public static void main(String args[]) {
		short pin = 1234;
		int number[] = {1,2,3,4,5,8,9,5,2};
		
		Telefono myPhone = new Telefono();
		Telefono agenda[] = null;
		
		//Datp agregadp  
		myPhone.pin = pin;
		
		//Dato agregado sobre otro dato compuesto
		agenda[0] = new Telefono(pin,number);
		
		
	}
}
