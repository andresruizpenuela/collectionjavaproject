package tipo_datos;

public class DatosAtomicos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte entero1=1;
		short entero2 = 3;
		char char1 = 'a';
		double real1 = 1.301;
		float real2 = 1.3F;
		
		System.out.println("entero 1= "+entero1);
		System.out.println("entero 2= "+entero2);
		System.out.println("character 1= "+char1);
		System.out.println("real 1 = "+real1);
		System.out.println("real 2 = "+real2);
		
		
		//operaciones
		System.out.println("entero 1 + entero 2= "+(entero1+entero2));
	}

}
