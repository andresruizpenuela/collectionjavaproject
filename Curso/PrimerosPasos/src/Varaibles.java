
public class Varaibles {

	/*
	 * 
	   Tipo		Tama�o	Valor m�nimo			Valor m�ximo
		byte	8 bits	-128					127
		short	16 bits	-32768					32767
		int		32 bits	-2147483648				2147483647
		long	64 bits	-9223372036854775808	9223372036854775807
		float	32 bits	-3.402823e38			3.402823e38
		double	64 bits	-1.79769313486232e308	1.79769313486232e308
		char	16 bits	'\u0000'				'\uffff'
	 */
	public static void main(String[] args) {
		// Comentario
		byte edad; //2^8  >> -128 a 127
		int dniNumero = 26499565; //2^32 >> -(2^32)/2 to (2^32)-1
		edad=35;
		System.err.println("Edad: "+edad);
		
		//------------------------------------
		
		char genero = 'M';
		byte _edad;
		_edad = 5;
		
		int numeroDni = 26499565;
		
		System.out.print("Genero: ");
		System.out.println(genero);
		System.out.println("Edad: "+_edad);
		System.out.println("Genero: "+genero+" Edad: "+_edad+" Numero DNI: "+numeroDni);
	}

}
