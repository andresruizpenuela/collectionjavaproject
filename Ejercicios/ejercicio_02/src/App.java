import java.util.Scanner;

import com.tda.estatica.pila.TDAPilaEstatica;

public class App {
    static TDAPilaEstatica operaciones;
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        
        operaciones = new TDAPilaEstatica();
        byte exit=0;
        
        do{
            System.out.print("Operaciones: "+
                "\n\t0- Salir"+
                "\n\t1- Sumar"+
                "\n\t2- Ver ultima operacion (peek)"+
                "\n\t3- Deshacer (pop)"+
                "\n\t4- Ver pila de operaciones"+
                "\nQue desea hacer: ");
            exit = sc.nextByte();
            switch (exit){
                case 0:
                    System.out.println("Adios");
                    break;
                case 1:
                    System.out.println("Sumar a+b");
                    sumar();
                break;
                case 2:
                    System.out.println("Ultima operacion: "+operaciones.peek());
                break;
                case 3:
                    System.out.println("Operacion desecha, valor borrado: "+operaciones.pop());
                break;
                case 4:
                    System.out.println(operaciones.toString());
                break;
                default:
                    System.out.println("No se reconoce la opcion");
            }
        }while(exit!=0);
    }

    public static void sumar(){
        System.out.print("Insertar a= ");
        Float a = sc.nextFloat();
        System.out.print("Insertar b= ");
        Float b = sc.nextFloat();

        operaciones.push(a+b);
    }
}

