package com.tda.estatica.pila;

import java.util.Arrays;

public class TDAPilaEstatica implements ITDAPIla{
    /************************************
     *Estrcutruca de datos
    ************************************/
    private Object[] elementData;
    private static final int LMAX = 3;
    private int tope;

    /************************************
     *Operaciones
    ************************************/

    /*
    * Constructor que crea un nuevo TDAPila
    * 
    */
    public TDAPilaEstatica(){
        this.elementData = new Object[this.LMAX];
        this.tope=0;
    }//Cierre del constructro
    

    
    /** 
     * Agrege un elemento a la Pila
     * Se puede producir OVERFLOW, es decri la PILA este llena y se intenta añadir elementos
     * @param data Elemento a añadir
     */
    public void push(Object data) {
        // OVERFLOW?
        if(this.tope<this.LMAX){//No
            this.elementData[this.tope]=data;
            this.tope++;
        }
    }

    
    /** 
     * @return Object
     */
    @Override
    public Object pop() {
        Object data = null;
        //UNDERFLOW?
        if(this.tope>1 && this.tope<=this.LMAX){//No
            this.tope--;
            data = this.elementData[this.tope];
            this.elementData[this.tope] = null;
            
        }
        return data;
    }

    
    /** 
     * @return Object
     */
    @Override
    public Object peek() {
        // TODO Auto-generated method stub
        return this.elementData[tope-1];
    }

    @Override
    public void isEmpaty() {
        // TODO Auto-generated method stub
        
    }

    
    /** 
     * @return int
     */
    @Override
    public int hasCode() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String toString(){
        return "Element: "+Arrays.toString(this.elementData)+", tope: "+this.tope;
    }
    
}
