package com.tda.estatica.pila;

public interface ITDAPIla {
    public void push(Object data);
    public Object pop();
    public Object peek();
    public void isEmpaty();
    @Override
    public String toString();
    public int hasCode();
}
