package clases.api.java;

import javax.swing.JOptionPane;

public class JOptionPaneUse {

	public static void main(String arg[]) {
		
		JOptionPane.showMessageDialog(null, "Welcom!", "App ARP", 
				JOptionPane.ERROR_MESSAGE);
		
		Object[] options = { "OK", "CANCEL" };
		int option = JOptionPane.showOptionDialog(null, "Click OK to continue", "Warning",
		             JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
		             null, options, options[0]);
		
		switch(option) {
			case 0:
				System.out.print("Hola!");
			break;
			case 1:
				System.out.print("Adios");
			break;
			default:
				System.out.print("Se dio a la x: "+option);
		}
		
		
		String inputValue = JOptionPane.showInputDialog("Please input a value");
		int edad = Integer.parseInt(inputValue);
		
		System.out.println("Edad: "+edad);
	}
}
