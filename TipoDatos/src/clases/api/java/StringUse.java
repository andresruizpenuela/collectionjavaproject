package clases.api.java;

public class StringUse {
	
	public static void main(String[] args) {
		String nombre = "Andres";
		
		System.out.println("Nombre = "+nombre);
		System.out.println("Longitud = "+nombre.length());
		
		System.out.println("Capital = "+nombre.charAt(0));
		System.out.println("Capital = "+nombre.charAt(nombre.length()-1));
		
		char nombre2[] = {'R','A','M','O','N'};
		String x = nombre.copyValueOf(nombre2);
		System.out.println("CopyOf=> Nombre =  "+x);
		
		String frase = " Hola Mundo! ";
		System.out.println("Frase = "+frase+".");
		System.out.println("Frase sin espacios= \""+frase.replace(" ", "")+"\".");
		System.out.println("Frase sin al principio o la final= \""+frase.strip()+"\".");
		
		frase = "Hoy es un estupendo d�a para apredner a prograr en Java";
		System.out.println("Frase: "+frase);
		System.out.println("Mitad de la frase: "+frase.substring(0,frase.length()-1/2));
		
		String alumno1,alumno2,alumno3;
		alumno1 = "Andres";
		alumno2 = "Ram�n";
		alumno3 = "andres";
		
		System.out.println("Alumno 1: "+alumno1+", Alumno 2"+alumno2+", Alumno 3"+alumno3);
		System.out.println("Alum1 == Alum2"+alumno1.equals(alumno2));
		System.out.println("Alum1 == Alum3"+alumno1.equals(alumno3));
		System.out.println("Alum1 == Alum3 (igonre capital letter)"+alumno1.equalsIgnoreCase(alumno3));
		
		
		
	}
}
