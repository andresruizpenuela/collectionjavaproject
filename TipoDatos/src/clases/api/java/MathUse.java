package clases.api.java;



public class MathUse {
	
	public void main(String[] args) {
		
		//Calcular la raiz cuadrado de un numero
		double raiz = Math.sqrt(3);
		System.out.println(raiz);
		
		//Redondeo
		int resultadoA = Math.round(1.0F);
		double num1 = 5.85;
		long resultadoB = Math.round(num1);
		System.out.println("resultadoA= "+resultadoA);
		System.out.println("resultadoB="+resultadoA);
		
		//Refundición
		resultadoA = (int)Math.round(num1);
		System.out.println("resultadoA (reunfuncion)= "+resultadoA);
		
		
		//Elevar potencia 5^3
		double base = 5;
		double exponente = 3;
		int resultado = (int)Math.pow(base, exponente);
		System.out.println(base+"^"+exponente+"="+resultado);
		
	}

}
