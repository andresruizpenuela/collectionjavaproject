package dato.registro;

import java.util.Arrays;

public class RegistroUse {
	//Por combenio, los nombres de los datos enuemrados se poenen con el primer digito en may�sucla
	enum Fruta{Sandia, Pera, Manzana};
	enum Level {LOW,MEDIUM, HIGH};
	
	public static void main(String[] args) {
		Fruta myCompra = Fruta.Sandia;
		
		
		Fruta valoresFruta[] = Fruta.values(); 
		System.out.println(Arrays.toString(valoresFruta));
		Arrays.stream(valoresFruta).forEach(f->{
			System.out.println(f);
		});
		
		Level myVar = Level.MEDIUM;
		switch(myVar) {
	      case LOW:
	        System.out.println("Low level");
	        break;
	      case MEDIUM:
	         System.out.println("Medium level");
	        break;
	      case HIGH:
	        System.out.println("High level");
	        break;
	    }
	}

}
