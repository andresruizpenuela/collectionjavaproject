package dato.abstracto.dinamico;

/*
 * La clase Nodo representa la estrcutra de datos para formar listas enalazadas, 
 * pilas dinamicas, etc.
 */
public class Nodo{
	
	private Object elementDada;
	private Nodo next; 
	
	//Constrcutror - Crea una instnacia de Nodo
	public Nodo() {
		this.elementDada = null;
		this.next = null;
	}

	//M�todos Getters && Setters
	public Object getElementDada() {
		return elementDada;
	}

	public void setElementDada(Object elementDada) {
		this.elementDada = elementDada;
	}

	public Nodo getNext() {
		return next;
	}

	public void setNext(Nodo next) {
		this.next = next;
	}
	
	public String toString() {
		return "dato = "+getElementDada()+", next = "+getNext();
	}

}
