package dato.abstracto.dinamico.pila;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import dato.abstracto.dinamico.Nodo;

//http://www.it.uc3m.es/java/2012-13/units/pilas-colas/guides/4/guide_es_solution.html
public class PilaEnlazada implements IPila{
	private Nodo elemento;
	private int cima; //Corresponde con el numero de elementos
	
	public PilaEnlazada() {
		this.elemento = null;
		this.cima = 0;
	}

	@Override
	public boolean isEmpaty() {
		// TODO Auto-generated method stub
		return (this.elemento==null && this.cima==0)?true:false;
	}

	@Override
	public void push(Object dato) {
		//

		//System.out.println("dato = "+dato);
		if(isEmpaty()){
			
			this.elemento = new Nodo();
			this.elemento.setElementDada(dato);
			this.elemento.setNext(null);
		}else{
			Nodo aux = new Nodo();
			aux.setElementDada(dato);
			aux.setNext(this.elemento);
			this.elemento = aux;
		}
		this.cima++;

	}

	@Override
	public Object pop() {
		Object dato = null;
		Nodo aux;
		if(!isEmpaty() && this.cima>0) {
			aux = this.elemento; // Liberar recursos
			
			//Obentemos el dato y pasamos al siguiente
			dato = this.elemento.getElementDada();
			this.elemento = this.elemento.getNext();
			//Reducimos la lista
			this.cima--;

			
			//Libermos recursos
			aux.setElementDada(null);
			aux.setNext(null);
			
			
		}

		return dato;
	}

	@Override
	public Object peek() {
		if(!isEmpaty())
			return this.elemento.getElementDada();
		
		return null;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		do {
			System.out.print("Destoy - "+pop());
		}while(this.elemento!=null);
		
		this.cima=0;
	}

	@Override
	public String toString() {
		if(isEmpaty()) {
			return "Vacia";
		}
		
		Object[] elementos = new Object[this.cima];
		Nodo aux = this.elemento;
		for(int i=0;i<this.cima;i++){
			elementos[i]=aux.getElementDada();
			aux = aux.getNext();
		}
		
		return "Elemento = { "+Arrays.toString(elementos)+" }"+", cima = "+this.cima;
	}
	
	
}
