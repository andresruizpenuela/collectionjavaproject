package dato.abstracto.dinamico.pila;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class PilaDinamiaTest {
	static PilaEnlazada stack;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		stack = new PilaEnlazada();
	}

	@Test
	public void test() {
		System.out.println("Show stack:: "+stack.toString());
		System.out.println("Vacia? "+stack.isEmpaty());
		System.out.println("Push -> 3,2,1");
		stack.push(3);
		stack.push(259);
		stack.push(5);
		System.out.println("Vacia? "+stack.isEmpaty());
		System.out.println("Show stack:: "+stack.toString());
		System.out.println("POP -> "+stack.pop());
		System.out.println("Show stack:: "+stack.toString());
		
		System.out.println("Peek -> "+stack.peek());
		System.out.println("Show stack:: "+stack.toString());
		
		System.out.println("Push -> 3");
		stack.push(3);
		System.out.println("Show stack:: "+stack.toString());
		
		
		System.out.println("POP -> "+stack.pop());
		System.out.println("POP -> "+stack.pop());
		System.out.println("POP -> "+stack.pop());
		System.out.println("Show stack:: "+stack.toString());
		System.out.println("POP -> "+stack.peek());
		System.out.println("Show stack:: "+stack.toString());
		
		
		
		System.out.println("Push -> 3,2,1");
		stack.push(3);
		stack.push(259);
		stack.push(5);
		stack.destroy();
		System.out.println("Show stack:: "+stack.toString());
	}

}
