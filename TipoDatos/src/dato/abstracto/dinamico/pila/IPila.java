package dato.abstracto.dinamico.pila;

public interface IPila {
	
	
	/**
	 * Metodo que comprueba si la pila esta se ha creado y esta vac�a
	 * Pre-Condicion: La pila debe estar inicializada.
	 * 
	 * @return True si esta vacia la pila, False resto de casos
	 */
	public boolean isEmpaty();
	
	/**
	 * Metodo para anaidr un elmento a la pila.
	 * <p>
	 * Pre-Condicion: La pila debe estar inicializada.
	 */
	public void push(Object dato);
	
	/**
	 * Metodo para quitiar el ultimo elemento a la pila.
	 * <p>
	 * Pre-Condicion: La pila debe estar inicializada.
	 * Pre-Condicion: La pila no debe estar vacia.
	 */
	public Object pop();

	/**
	 * Metodo para ver el ultimo elemento a la pila.
	 * <p>
	 * Pre-Condicion: La pila debe estar inicializada.
	 * Pre-Condicion: La pila no debe estar vacia.
	 */
	public Object peek();
	
	/**
	 * Metodo para borrar la pila
	 */
	public void destroy();
}
