package dato.abstracto.dinamico.cola;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class QueueEnlazadaTest {
	
	static QueueEnlazada queue;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		queue = new QueueEnlazada();
	}

	@Test
	public void test() {
		System.out.println("\nStart");
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("toString => "+queue.toString());
		
		System.out.println("\nOperaciones con un solo elemnto en la cola");
		System.out.println("\nPush 3");
		queue.push(3);
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("toString => "+queue.toString());
		System.out.println("Front => "+queue.front());
		
		
		System.out.println("\nInsertemos un elmeento a la cola");
		queue.push(53);
		queue.push(25);
		System.out.println("toString => "+queue.toString());
		System.out.println("Front => "+queue.front());
		
		/*
		System.out.println("\nOperacioens con un solo elemnto en la cola");
		System.out.println("\nPush 3");
		queue.push(3);
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("toString => "+queue.toString());
		System.out.println("\nFront => "+queue.front());
		System.out.println("\nPop => "+queue.pop());
		System.out.println("toString => "+queue.toString());
		
		System.out.println("\nOperacioens con varios elementos en la cola");
		System.out.println("\nPush 6, 2, 1, 0");
		queue.push(6);
		queue.push(2);
		queue.push(1);
		queue.push(0);
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("toString => "+queue.toString());
		
		System.out.println("\nFront => "+queue.front());
		System.out.println("Front => "+queue.front());
		
		System.out.println("\nPop => "+queue.pop());
		System.out.println("toString => "+queue.toString());
		System.out.println("Push 10");
		queue.push(10);
		System.out.println("toString => "+queue.toString());
		
		System.out.println("\nPop => "+queue.pop());
		System.out.println("Pop => "+queue.pop());
		System.out.println("Pop => "+queue.pop());
		System.out.println("Pop => "+queue.pop());
		System.out.println("toString => "+queue.toString());
		
		
		//Queue vacia
		System.out.println("\nOperacioens con la cola vacia");
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("Front => "+queue.front());
		System.out.println("Pop => "+queue.pop());
		System.out.println("toString => "+queue.toString());
		
		//Destroy
		System.out.println("\nOperacion Destroy");
		System.out.println("\nPush 6, 2, 1, 0");
		queue.push(6);
		queue.push(2);
		queue.push(1);
		queue.push(0);
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("toString => "+queue.toString());
		System.out.println("Destoy");
		queue.destroy();
		System.out.println("Vacia? "+queue.isEmpaty());
		System.out.println("toString => "+queue.toString());
		*/
		
	}

}
