package dato.abstracto.dinamico.cola;

import java.util.Arrays;

import dato.abstracto.dinamico.Nodo;

public class QueueEnlazada implements ICola {
	private Nodo firstElement;
	private Nodo endElement;
	private int length;
	
	public QueueEnlazada() {
		this.firstElement = null;
		this.endElement = null;
		this.length = 0;
	}

	@Override
	public boolean isEmpaty() {
		// TODO Auto-generated method stub
		return (this.length!=0 && this.firstElement!=null)?false:true;
	}

	@Override
	public void push(Object dato) {
		// TODO Auto-generated method stub
		Nodo n1 = new Nodo();
		n1.setElementDada(dato);
		n1.setNext(null);
		
		if(isEmpaty()) {
			this.firstElement = this.endElement = n1;
			
		}else {
			this.endElement.setNext(n1);
			this.endElement = n1;
		}
		this.length++;
		
		//this.length++;
		/*
		if(isEmpaty()) {
			// primero = ulitmo = n1
			//pirmero -> ultimo -> null
			
			this.firstElement=n1;
			this.endElement=n1;
			this.firstElement.setElementDada(this.endElement);
		}else {
			Nodo aux = this.firstElement;
			n1.setNext(aux);
			this.endElement=n1;
		}
		this.length++;
		*/
	}

	@Override
	public Object pop() {
		Nodo aux;
		Object data = null;

		
		return data;
	}

	@Override
	public Object front() {
		// TODO Auto-generated method stub
		return (!isEmpaty())?this.firstElement.getElementDada():null;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		while(this.length>0) {
			pop();
		}
	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return false;
	}
	
	 public String toString() {
		 System.out.println("=========================================");
		 System.out.println("To String - Queue");
		 System.out.println("\tLegnth= "+this.length);
		 System.out.println("\tFirst Element=> "+this.firstElement);
		 System.out.println("\tEnd Element=> "+this.endElement);
		 System.out.println("\tFirst Element.equals(EndElement)=> "+
				 (this.firstElement==this.endElement));
		 Object[] elem = new Object[this.length];
		 Nodo aux = this.firstElement;
		 int it = 0;
		 System.out.println("\t- Elements: ");
		 while(aux!=null) {
			 System.out.println("\t\tit= "+it+" aux=[ "+aux+"]");
			 elem[it] = aux.getElementDada();
			 aux = aux.getNext();
			 it++;
		 }
		 System.out.println("=========================================");
		 return "Element of firts to the last= "+Arrays.toString(elem);
	 }
	

}


