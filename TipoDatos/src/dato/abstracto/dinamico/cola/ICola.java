package dato.abstracto.dinamico.cola;

public interface ICola {
	public boolean isInit();
	public boolean isEmpaty();
	public void push(Object dato);
	public Object pop();
	public Object front();
	public void destroy();
    public String toString();

}
