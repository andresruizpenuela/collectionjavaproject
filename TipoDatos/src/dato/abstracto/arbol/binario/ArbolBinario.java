package dato.abstracto.arbol.binario;

public class ArbolBinario {
	NodoBinario raiz;
	
	public ArbolBinario() {
		raiz = null;
		
	}
	
	
	public void insertar (int i, Object contenido) {
		NodoBinario n = new NodoBinario(i);
		n.data = contenido;
		
		if(raiz==null) {
			//No hay nodos
			raiz = n;
		}else {
			//Recorremos los nodas existens para ubicarlo
			NodoBinario aux = raiz; //Esca�,ps e� arbp�
			
			//Buscamos el nodo padre donde se ubicar�
			while(aux!=null) {
				n.padre = aux;
				if(n.key >= aux.key) {
					//Buscmo el nodo hijo de la derecha
					aux = aux.hijoDer;
				}else {
					//Buscamos el nodo hijo de laizquierda
					aux = aux.hijoIzq;
				}
			}
			
			//Inserccion del nodo
			if(n.key < n.padre.key) {
				//Es el nodo hijo izquierda
				n.padre.hijoIzq = n;
			}else {
				//Es e� mpdp hijo derecja
				n.padre.hijoDer = n;
			}
		}
	}
	
	// Recorrer el aborl in-order (de menor a mayor)
	public void recorrer(NodoBinario nodo) {
		if(nodo!=null) {
			recorrer(nodo.hijoIzq);
			System.out.println("Indice= "+nodo.key+" contneido= "+nodo.data);
			recorrer(nodo.hijoDer);
		}
	}
	
	// Recorrer el aborl in-order invertido (de menor a mayor)
		public void recorrerInvert(NodoBinario nodo) {
			if(nodo!=null) {
				recorrer(nodo.hijoDer);
				System.out.println("Indice= "+nodo.key+" contneido= "+nodo.data);
				recorrer(nodo.hijoIzq);
			}
		}
	
	
}
