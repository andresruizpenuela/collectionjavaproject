package dato.abstracto.arbol.binario;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ArbolBinarioTest {
	public static ArbolBinario arbol;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		arbol = new ArbolBinario();
	}

	@Test
	public void test() {
		arbol.insertar(0, "Manzana");
		arbol.insertar(8, "Pera");
		arbol.insertar(1, "Platano");
		arbol.insertar(9, "Melon");
		arbol.insertar(5, "Sandia");
		
		System.out.print("Recorrido in order\n");
		arbol.recorrer(arbol.raiz);
		
		System.out.print("Recorrido in order solo los nodos hijos izquirda\n");
		arbol.recorrer(arbol.raiz.hijoIzq);
		
		System.out.print("Recorrido in order solo los nodos hijos derecha\n");
		arbol.recorrer(arbol.raiz.hijoDer);
		
		System.out.print("Recorrido in order invertida\n");
		arbol.recorrerInvert(arbol.raiz);
	}

}
