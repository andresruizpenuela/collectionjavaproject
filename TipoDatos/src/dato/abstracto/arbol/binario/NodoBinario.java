package dato.abstracto.arbol.binario;

public class NodoBinario {
	public NodoBinario padre;	// Nodo padre
	public NodoBinario hijoIzq;	// Aquellos nodos que hijo.key > padre.key
	public NodoBinario hijoDer;	// Aqeullos nodos que hijo.key > padre.key
	public int key; 		// Identificador del nodo
	public Object data; 	// Contenido del nodo
	
	//Constructor por defecto (inicializar un nodo
	public NodoBinario(int indice) {
		key=indice;
		hijoDer = null;
		hijoIzq = null;
		padre = null;
		data = null;
	}

}
