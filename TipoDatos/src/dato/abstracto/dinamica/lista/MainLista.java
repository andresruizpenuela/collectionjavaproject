package dato.abstracto.dinamica.lista;

public class MainLista {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ListaEnlazada l1;
		
		System.out.println("Init");
		l1 = new ListaEnlazada();
		System.out.print("INIT: "+l1.isEmpaty());
		
		System.out.println("Insertar datos");
		l1.agregarAlInicio("Andres");
		System.out.println("inicio=> contenido= "+l1.inicio.contenido+", siguiente= "+l1.inicio.siguiente);
		System.out.println("fin=> contenido= "+l1.fin.contenido+", siguiente= "+l1.fin.siguiente);
		
		
		l1.agregarAlInicio("Juan");
		System.out.println("inicio=> contenido= "+l1.inicio.contenido+", siguiente= "+l1.inicio.siguiente);
		System.out.println("fin=> contenido= "+l1.fin.contenido+", siguiente= "+l1.fin.siguiente);
		
		l1.agregarAlInicio("Ramon");
		System.out.println("inicio=> contenido= "+l1.inicio.contenido+", siguiente= "+l1.inicio.siguiente);
		System.out.println("fin=> contenido= "+l1.fin.contenido+", siguiente= "+l1.fin.siguiente);
		
		
		l1.agregarAlIFinal("Pedro");
		System.out.println("inicio=> contenido= "+l1.inicio.contenido+", siguiente= "+l1.inicio.siguiente);
		System.out.println("fin=> contenido= "+l1.fin.contenido+", siguiente= "+l1.fin.siguiente);
		
	}

}
