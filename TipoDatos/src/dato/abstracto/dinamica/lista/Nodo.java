package dato.abstracto.dinamica.lista;

public class Nodo {

	public Object contenido;
	public Nodo siguiente;
	
	public Nodo() {
		contenido = null;
		siguiente = null;
	}
	
	public Nodo(Object data) {
		contenido = data;
		siguiente = null;
	}
	
	public Nodo(Object data, Nodo nodo) {
		this.contenido = data;
		this.siguiente = nodo;
	}
}
