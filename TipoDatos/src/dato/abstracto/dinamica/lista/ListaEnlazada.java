package dato.abstracto.dinamica.lista;

public class ListaEnlazada {

	Nodo inicio, fin;
	
	public ListaEnlazada() {
		inicio = null;
		fin = null;
	}
	
	//Si el nodo incio de la lista es nulo, la lista esta vacia
	public boolean isEmpaty() {
		return (inicio==null)?true:false;
	}
	
	//Agrega un nodo al principio de la lista null->...->C->B->A
	public void agregarAlInicio(Object dato) {
		Nodo nodo = new Nodo(dato);
		
		if(isEmpaty()) {
			//inicio = new Nodo(dato,inicio); //Es lo mimosmo 
			inicio = nodo;
			fin = inicio; //Contiente el ultimo elmento insertado
		}else {
			//inicio = new Nodo(dato,inicio); //Es lo mimosmo 
			nodo.siguiente=inicio;
			inicio = nodo;
		}
		
	}
	
	//Agrega un nodo al final de la lista A->B->C->...->null
	public void agregarAlIFinal(Object dato) {
		
		if(isEmpaty()) {
			fin = new Nodo(dato,fin);
			inicio = fin;
		}else {
			fin = new Nodo(dato,fin);
		}
	}	
}
