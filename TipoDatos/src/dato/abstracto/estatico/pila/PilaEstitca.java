package dato.abstracto.estatico.pila;

import java.util.Arrays;

/**
 * TDA Pila Estica.
 * 
 * Una pila (stack en ingl�s) es una estructura de datos lineal 
 * que solo tienen un �nico punto de acceso fijo por el cual se a�aden, 
 * eliminan o se consultan elementos. El modo de acceso a los elementos 
 * es de tipo LIFO (del ingl�s Last In First Out, �ltimo en entrar, primero 
 * en salir)
 * 
 * 
 * @author Andr�s Ruiz Pe�uela
 * @version 0.1
 *
 */
public class PilaEstitca implements IPila {
	
	/*************************
	 ** Estrucutra de datos **
	**************************/
	//Array de elementos
	private Object[] elemenData=null;
	//Tama�o m�ximo de la Pila
	private static final int LMAX = 2;
	//Numero de elementos agregados en la cola,
	//-1 => No inicializada
	//[0...LMAX] => Elemenots que contiente la pila
	private int tope = -1; 
	
	
	/*************************
	 **     Operaicones     **
	**************************/
	
	/**
	 * Constructor por defecto, crea una nueva pila
	 */
	public PilaEstitca() {
		//Crea e inicicilaliza los elementos
		this.elemenData = new Object[this.LMAX];
		this.tope=0; 
	}

	@Override
	public boolean isEmpaty() throws Exception {
		if(isInit()) {
			return (this.tope==0)?true:false;
		}
		throw new Exception("[PilaEstitca] - Pila no creada");
	}

	@Override
	public void push(Object dato) throws Exception {
		// TODO Auto-generated method stub
		if(!isInit())
			throw new Exception("[PilaEstitca] - StackNotCreated");
		
		if(this.tope<this.LMAX) {
			this.elemenData[this.tope]=dato;
			this.tope++;
		}else {
			throw new Exception("[PilaEstitca] - StackOverflow");
		}
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		this.elemenData=null;
		this.tope=-1;
	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return (this.elemenData!=null&& this.tope>=0)?true:false;
	}
	
	
	public String toString() {
		return "Element="+Arrays.toString(this.elemenData)+"tope= "+this.tope;
		
	}

	@Override
	public Object pop() throws Exception {
		if(!isInit())
			throw new Exception("[PilaEstitca] - StackNotCreated");
		
		if(isEmpaty())
			throw new Exception("[PilaEstitca] - StackIsEmpaty");
		
		Object data=null;
		if(this.tope>=1){
			this.tope--;
			data = this.elemenData[this.tope];
			this.elemenData[this.tope]=null;
		}
		return data;
	}

	@Override
	public Object peek() throws Exception {
		if(!isInit())
			throw new Exception("[PilaEstitca] - StackNotCreated");
		
		if(isEmpaty())
			throw new Exception("[PilaEstitca] - StackisEmpaty");


		return this.elemenData[this.tope-1];
	}
}
