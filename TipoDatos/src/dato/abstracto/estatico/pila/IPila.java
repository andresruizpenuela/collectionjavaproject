/**
 * 
 */
package dato.abstracto.estatico.pila;

/**
 * Interfaz que nombra las operaciones basicas del TDA Pila
 * 
 * @author Andr�s Ruiz pe�uela
 * @version 0.1
 * @see <href="http://codigolibre.weebly.com/blog/pilas-en-java"><Pilas en Java/href>
 */
public interface IPila {
	/**
	 * Metodo que comprueba si la pila esta se ha creado o inicilaizado.
	 * 
	 * @return True si la pila se ha creado, Flase el resto de casos
	 */
	public boolean isInit();
	
	
	/**
	 * Metodo que comprueba si la pila esta se ha creado y esta vac�a
	 * Pre-Condicion: La pila debe estar inicializada.
	 * 
	 * @return True si esta vacia la pila, False resto de casos
	 * @throws Exception Codigo del error StackNotCreated Cuasa del error Si la pila no se ha creado
	 */
	public boolean isEmpaty() throws Exception;
	
	/**
	 * Metodo para anaidr un elmento a la pila.
	 * <p>
	 * Pre-Condicion: La pila debe estar inicializada.
	 * 
	 * @param dato Elemento para agregar a la pila
	 * @throws Exception Codigo del error StackNotCreated Causa del error Si la pila no se ha creado
	 * @throws Exception Codigo del error StackOverflow Causa del error Si la pila esta llena
	 */
	public void push(Object dato) throws Exception;
	
	/**
	 * Metodo para quitiar el ultimo elemento a la pila.
	 * <p>
	 * Pre-Condicion: La pila debe estar inicializada.
	 * Pre-Condicion: La pila no debe estar vacia.
	 * 
	 * @throws Exception Codigo del error StackNotCreated Causa del error Si la pila no se ha creado
	 * @throws Exception Codigo del error UnderFlow Causa del error Si la pila esta vacia
	 */
	public Object pop() throws Exception;

	/**
	 * Metodo para ver el ultimo elemento a la pila.
	 * <p>
	 * Pre-Condicion: La pila debe estar inicializada.
	 * Pre-Condicion: La pila no debe estar vacia.
	 * 
	 * @throws Exception Codigo del error StackNotCreated Causa del error Si la pila no se ha creado
	 * @throws Exception Codigo del error UnderFlow Causa del error Si la pila esta vacia
	 */
	public Object peek() throws Exception;

	
	/**
	 * Metodo para borrar la pila
	 */
	public void destroy();

	
}
