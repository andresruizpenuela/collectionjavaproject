/**
 * 
 */
package dato.abstracto.estatico.pila;


import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * @author andre
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PilaEstaticaTest {
	
	static PilaEstitca nombresJugadores;
	/**
	 * Creamos la pila
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("********\n setUp \n********");
		
		if(nombresJugadores!=null) {//Pila no inicializada
			System.out.println("Pila vacia: "+nombresJugadores.isEmpaty());
		}
		nombresJugadores = new PilaEstitca();
		System.out.println("Pila vacia: "+nombresJugadores.isEmpaty());
			
		
	}

	/**
	 * Test que borra una pila y la vuelve a crear.
	 */
	
	@Test
	public void test1TestDestoy() {
		System.out.println("********\n test1_TestDestoy \n********");
		try {
			System.out.println("Pila vacia?: "+nombresJugadores.isEmpaty());
			nombresJugadores.destroy();
			System.out.println("Pila vacia?: "+nombresJugadores.isEmpaty());
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			nombresJugadores = new PilaEstitca();
			try {
				System.out.println("Pila  vacia?: "+nombresJugadores.isEmpaty());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	@Test
	public void test2TestPush() {
		System.out.println("********\n test2_TestPush \n********");
		try {
			nombresJugadores.push(1);
			nombresJugadores.push(2);
			nombresJugadores.push(3); //Generamos un OverFlow
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(nombresJugadores.toString());
	}

	@Test
	public void test3TestPop(){
		System.out.println("********\n test3_TestPop \n********");
		System.out.println(nombresJugadores.toString());
		try {
			System.out.println("Last element: "+nombresJugadores.pop());
			System.out.println(nombresJugadores.toString());
			System.out.println("Last element: "+nombresJugadores.pop());
			System.out.println(nombresJugadores.toString());
			System.out.println("Last element: "+nombresJugadores.pop()); //Genermos un UnderFlow
			System.out.println(nombresJugadores.toString());
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(nombresJugadores.toString());
	}

	@Test
	public void test4TestPeek(){
		System.out.println("********\n test4_TestPeek \n********");
		System.out.println(nombresJugadores.toString());
		try {
			System.out.println("Last element: "+nombresJugadores.peek());
			System.out.println(nombresJugadores.toString());
			System.out.println("Last element: "+nombresJugadores.pop());
			System.out.println(nombresJugadores.toString());
			System.out.println("Last element: "+nombresJugadores.peek()); //Genermos un UnderFlow
			System.out.println(nombresJugadores.toString());
		}catch(Exception e) {
			e.printStackTrace();
			try {
				//Si la pila estubiera vacia o no inicializada
				nombresJugadores = new PilaEstitca();
				nombresJugadores.push(1);
				nombresJugadores.push(2);
				System.out.println(nombresJugadores.toString());
				System.out.println("Last element: "+nombresJugadores.peek());
				System.out.println(nombresJugadores.toString());
				System.out.println("Last element: "+nombresJugadores.pop());
				System.out.println(nombresJugadores.toString());
				System.out.println("Last element: "+nombresJugadores.peek()); //Genermos un UnderFlow
				System.out.println(nombresJugadores.toString());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
		}
		System.out.println(nombresJugadores.toString());
	}
}
