package dato.abstracto.estatico.cola;

import java.util.Arrays;
/*
 * Cola estatica v0
 * Poco eficiente porque al quitar un elemento se ha de mover todos los elementos de la misma
 * ya sea mediante bucles for o por copia de arrays
 */
public class ColaEstatica implements ICola{
	private Object[] elementData; //Elementos de la cola
	private static final int LMAX = 3; //Longitud maxima de cola
	private int nelem; //Numeros de elementos en cola

	//Constructor
	public ColaEstatica() {
		this.elementData = new Object[this.LMAX];
		nelem=0;
	}
	
    @Override
    public boolean isInit() {
        // TODO Auto-generated method stub
        return (this.elementData==null || this.nelem== (Integer) null)?false:true;
    }

    @Override
    public boolean isEmpaty() {
        // TODO Auto-generated method stub
        return (nelem==0)?true:false;
    }

    @Override
    public void push(Object dato){
        // TODO Auto-generated method stub
    	if(this.nelem<this.LMAX) {
    		this.elementData[this.nelem]=dato;
    		this.nelem++;
    	}
    }

    @Override
    public Object pop() {
        // TODO Auto-generated method stub
    	Object dato = null;
    	if(this.nelem>0) {
    		dato = this.elementData[0];
    		//aux es un vecto copia de la desde la poscioi 1 hasta la posicion nelem-1
    		Object[] aux = Arrays.copyOfRange(this.elementData, 1, this.nelem);
    		this.nelem--;
    		this.elementData= Arrays.copyOf(aux, this.LMAX);
    	}
    	return dato;
        
    }

    @Override
    public Object front() {
        // TODO Auto-generated method stub
    	Object dato = null;
    	if(this.nelem>0) {
    		dato = this.elementData[0];
    	}
        return dato;
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
    	this.elementData=null;
    	this.nelem=(Integer) null;
        
    }
    
    @Override
    public String toString() {
    	return "Element= "+Arrays.toString(this.elementData)+", nElem= "+this.nelem;
    }
    
}
