/**
 * 
 */
package dato.abstracto.estatico.cola.ciclica;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author andre
 *
 */
public class ColaEstaticaCiclicaTest {
	static ColaEstaticaCiclica queue;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		queue = new ColaEstaticaCiclica();
		queue.push(1);
		queue.push(2);
		queue.push(3);
		queue.push(6);
	}

	@Test
	public void test() {
		System.out.println("\n-----------\n");
		System.out.println(queue.toString());
		System.out.println("Front= "+queue.front());
		System.out.println("Pop= "+queue.pop());
		System.out.println(queue.toString());


		//
		System.out.println("\n-----------\n");
		System.out.println(queue.toString());
		System.out.println("Pop= "+queue.pop());
		queue.push(5);
		System.out.println("Push 5=> "+queue.toString());
		System.out.println("Pop= "+queue.pop());
		System.out.println(queue.toString());
		System.out.println("Pop= "+queue.pop());
		System.out.println(queue.toString());
		System.out.println("Pop= "+queue.pop());
		System.out.println(queue.toString());
		


		//
		System.out.println("\n-----------\n");
		System.out.println(queue.toString());
		queue.push(5);
		System.out.println("Push 5=> "+queue.toString());
		queue.push(2);
		System.out.println("Push 2=> "+queue.toString());
		queue.push(1);
		System.out.println("Push 1=> "+queue.toString());
		queue.push(10);
		System.out.println("Push 10=> "+queue.toString());


		//
		System.out.println("\n-----------\n");
		System.out.println(queue.toString());
		System.out.println("Front= "+queue.front());
		System.out.println("Front= "+queue.front());
		System.out.println("Front= "+queue.front());

		//
		System.out.println("\n-----------\n");
		System.out.println(queue.toString());
		System.out.println("Pop= "+queue.pop()+", Front= "+queue.front());
		System.out.println("Pop= "+queue.pop()+", Front= "+queue.front());
		System.out.println("Pop= "+queue.pop()+", Front= "+queue.front());
		System.out.println("Pop= "+queue.pop()+", Front= "+queue.front());
	}

}
