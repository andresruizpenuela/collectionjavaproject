package dato.abstracto.estatico.cola.ciclica;

import java.util.Arrays;

import dato.abstracto.estatico.cola.ICola;

/**
* Cola estatica v1: Cola Circular
* Algo mas eficiente a costa de mayor recursos de memoria, al no tener que despalzar
* los elemneots de la cola al quitar un elemento.
* Para ello utilizamos el siguiente algoritmo
* 
* Inserccion: Condicion que usados<tama�om, ai no se produce OVERFLOW
* 	cola[(primero+usado)%tama�o]=valor;
* 	usados=usados++; o usados=(usados++)&tama�o; //Para hacer que este entre en el intervalor [0,TAMA�O-1]
* 
* Extraemos: Condicion que usados>0, si no se produce UNDERLOW
* 	valor=cola[primero];
*   cola[primero]=null; //opcional si se desea limpiar el elemento
* 	primero=(primero++)%tama�o; //Para hacer que este entre en el intervalor [0,TAMA�O-1]
* 	usados=(usados--)&tama�o; primero=(primero++)%tama�o; 
* 
* 
* Ejemplo practico del algoritmo
* 
* Iteraciones:
* 
* a)Insertamos dato=2
* Datos tama�a=3, primero=0, usados=0 y cola=[null,null,null]
* Insertamos dato=2--> 
* 	cola[(0+0)%3]=cola[0%3]=cola[0]=2 -> cola=[2.null.null]
* 	usaados=0+1=1
* 
* b)Insertamos dato=3
* Datos tama�a=3, primero=0, usados=1 y cola=[2,null,null]
* 	cola[(0+1)%3]=cola[1%3]=cola[1]=2 -> cola=[2.3.null]
* 	usaados=1+1=2
* 
* b)Insertamos dato=4
* Datos tama�a=3, primero=0, usados=2 y cola=[2,3,null]
* 	cola[(0+2)%3]=cola[2%3]=cola[2]=4 -> cola=[2.3.4]
* 	usados=2+1=3 -> usados=3mod3=0
* 
* c)Insertamos dato=50 (Caso OverFlow)
* Datos tama�a=3, primero=0, usados=3 y cola=[2,3,4]
* 	cola[(0+3)%3]=cola[3%3]=cola[0]=50 -> cola=[50.3.4] >>> Machaca el valor!! OVERFLOW
* 	usaados=1+1=4 -> usados=4mod3=1
* 
* Extracion:
* 
* a) Itereracion 1
* Datos tama�a=3, primero=0, usados=3 y cola=[2,3,4]
* 	valor = cola[primero]=cola[0]=2
*   cola[primero]=cola[0]=null -> cola=[null,3,4] //quitmoas elmento residual
*   primero=(0+1)%3=1%3=1;
*   usados=(3-1)%3=2%3=2;
*   
* b) Itereracion 2
* Datos tama�a=3, primero=1, usados=2 y cola=[null,3,4]
* 	valor = cola[primero]=cola[1]=3
*   cola[primero]=cola[1]=null -> cola=[null,null,4] //quitmoas elmento residual
*   primero=(1+1)%3=2%3=2;
*   usados=(2-1)%3=1%3=1;
*   
* c) Itereracion 3
* Datos tama�a=3, primero=2, usados=1 y cola=[null,null,4]
* 	valor = cola[primero]=cola[2]=4
*   cola[primero]=cola[2]=null -> cola=[null,null,null] //quitmoas elmento residual
*   primero=(2+1)%3=3%3=0;
*   usados=(1-1)%3=0%3=0;
*      
* c) Itereracion 4 (Caso underflow)
* Datos tama�a=3, primero=0, usados=0 y cola=[null,null,null]
* 	valor = cola[primero]=cola[0]=null --> Es null, lo que indica que esta vac�o, si no sladira un elmento residual UNDERFLOW
*   cola[primero]=cola[0]=null -> cola=[null,null,null]
*   primero=(0+0)%3=0%3=0; --> No se genra desplazamiento
*   usados=(0-0)%3=0%3=0; --> No se genra desplazamiento
*   
*   
*   
* Inserccion + Extracion + Insercion
* 
* 
* a) Insertamos 55
* Datos tama�a=3, primero=1, usados=1 y cola=[null,3,null]
* 	cola[(1+1)%3]=cola[2%3]=cola[2]=55 -> cola=[null.3.55] 
* 	usaados=1+1=2 -> usados=2mod3=2
* 
* b) Extraemos
* Datos tama�a=3, primero=2, usados=2 y cola=[null,3,55l]
* 	valor = cola[primero]=cola[2]=3
*   cola[primero]=cola[2]=null -> cola=[null,null,55] //quitmoas elmento residual
*   primero=(1+1)%3=2%3=2;  
*   usados=(2-1)mod3=1mod3=1
*   
* c) Insertamos 0
* Datos tama�a=3, primero=2, usados=1 y cola=[null,3,null]
* 	cola[(2+1)%3]=cola[3%3]=cola[0]=0 -> cola=[0.null.55] 
* 	usaados=1+1=2 -> usados=2mod3=2
* 
*/
public class ColaEstaticaCiclica implements ICola {
    //Estructura de datos
    private Object[] elementData;
    private int usados;
    private int primero;
    private static final int LMAX = 3;

    //Constructor
    public ColaEstaticaCiclica(){
        this.elementData = new Object[this.LMAX];
        this.usados=0;
        this.primero=0;
    }

    @Override
    public boolean isInit() {
        // TODO Auto-generated method stub
        return (this.elementData==null)?false:true;
    }
    @Override
    public boolean isEmpaty() {
        return (this.usados==0)?true:false;
    }
    @Override
    public void push(Object dato) {
        if(this.usados<this.LMAX){
            this.elementData[(this.primero+this.usados)%this.LMAX]=dato;
            this.usados=(this.usados+1)%(this.LMAX+1);
        }
        
    }
    @Override
    public Object pop() {
        Object dato = null;
        if(!isEmpaty()&&this.usados>0){
            dato = this.elementData[this.primero];
            this.elementData[this.primero]=null; //Eliminamos dato resitudal
            this.primero=(this.primero+1)%this.LMAX;
            this.usados--;
        }
        return dato;
    }
    @Override
    public Object front() {
        Object dato = null;
        if(!isEmpaty()){
            dato = this.elementData[this.primero];
        }
        return dato;
    }

    @Override
    public void destroy() {
       this.elementData = null;
       this.usados = 0;
       this.primero = 0;
        
    }

    public String toString(){
        return "Cola="+Arrays.toString(this.elementData)+", usados= "+this.usados+", primero= "+this.primero;
    }
    
}
