/**
 * 
 */
package dato.abstracto.estatico.cola;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author andre
 *
 */
public class ColaEstaticaTest {
	static ColaEstatica queue;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		queue = new ColaEstatica();
		queue.push(1);
		queue.push(2);
		queue.push(3);
	}

	@Test
	public void test() {
		System.out.println(queue.toString());
		System.out.println("Front= "+queue.front());
		System.out.println("Pop= "+queue.pop());
		System.out.println(queue.toString());
		
		
	}

}
